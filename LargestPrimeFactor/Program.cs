using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LargestPrimeFactor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var startTime = DateTime.Now;
            Console.WriteLine("Start Time: {0}", startTime);
            var input = 600851475143;
            var result = LargestPrimeFactor(input);
            
            var endTime = DateTime.Now;
            Console.WriteLine("End Time: {0}", endTime);
            Console.WriteLine("Total Time: {0}", endTime - startTime);
            Console.WriteLine("The largest prime factore of {0} is {1}", input, result);
            Console.ReadLine();
        }
    
        private static long LargestPrimeFactor(long input)
        {
            for(int x = 2; x <= (input / 2); x++)
            {
                if (IsAFactor(input, x))
                {
                    var factor = input / x;
                    Console.WriteLine("{0} - {1} is a factor of {2}", DateTime.Now, factor, input);
                    if (IsAPrimeNumber(factor))
                    {
                        Console.WriteLine("{0} - {1} is a prime number.", DateTime.Now, factor);
                        return factor;
                    }
                }
            }
            
            return 1;
        }
        
        private static bool IsAFactor(long input, long devisor)
        {
            var result = input % devisor == 0;
            return result;
        }
        
        private static bool IsAPrimeNumber(long number)
        {
            var counter = 2;
            while (counter < number)
            {
                if(IsAFactor(number, counter))
                {
                    Console.WriteLine("{0} is not a prime number", number);
                    Console.WriteLine("{0} is divisible by {1}", number, counter);
                    return false;
                }
                counter++;
            }   
            
            Console.WriteLine("{0} is a prime number", number);
            
            return true;
        }
    }
}
